// 读取文件判断重复行

package main

import (
	"os"
	"fmt"	
	"bufio"
)
// go run dup2.go ss.txt
func main() {
	counts := make(map[string]int)

	files := os.Args[1:]
	if len(files) == 0 {
		// 读取命令行输入的文本内容
		fmt.Println("无参数")
		countLines(os.Stdin, counts)
	}else {
		for _,arg := range files {
			fmt.Println(arg)
			f, err := os.Open(arg) 
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v \n", err)
				continue
			}

			countLines(f, counts)
			f.Close()
		}
	}

	for line,n := range counts {
		if n > 1 {
			fmt.Printf("%d \t %s \n", n, line)
		}
	}

}

func countLines(f *os.File, counts map[string]int) {
	input := bufio.NewScanner(f)

	for input.Scan() {
		//fmt.Println(input.Text())
		counts[input.Text()]++
	}
}
// Dup1 prints the text of each line that appears more than
// once in the standard input, preceded by its count.

package main

import (
	"fmt"
	"os"
	"bufio"
)

func main(){
	// map 存储了key/value的集合 key 可以是任意类型只要实现 == 运算符
	// 内置函数 make 创建空 map
	counts := make(map[string]int)
	// 继续来看bufio包，它使处理输入和输出方便又高效。Scanner类型是该包最有用的特性之一，它读取输入并将其拆成行或单词；通常是处理行形式的输入最简单的方法
	input := bufio.NewScanner(os.Stdin)

	for input.Scan() {
		counts[input.Text()] ++ 
	}

	for line,n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s",n,line)
		}
	}
}

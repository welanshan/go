package main

import (
	"os"	
	"fmt"
	"io/ioutil"
	"strings"
)

// 只读取文件 支持多个
func main(){

	counts := make(map[string]int)

	files := os.Args[1:]

	for _, arg := range files {
		// 读取文件 
		data, err := ioutil.ReadFile(arg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "dup3: %v\n", err)
            continue
		}
		
		data_str := string(data)
		for _,line := range strings.Split(data_str,"\n") {
			counts[line] ++ 
		}
	}


	for line,n := range counts {
		if n > 1 {
			//fmt.Println(line)
			//fmt.Printf("%d\t%s\n",n,line) // %s 不能位于开头位置？
			fmt.Printf("out:%s,%d\n",line,n)
		}
	}

}
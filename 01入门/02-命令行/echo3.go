package main

import (
	"os"
	"fmt"
	"strings"
)

// go run echo3.go 1 2 3 4
func main() {
	fmt.Println(strings.Join(os.Args[1:], " "))
	fmt.Println(strings.Join(os.Args, " ")) // C:\Users\16081844\AppData\Local\Temp\go-build2987022463\b001\exe\echo3.exe 1 2 3 4
}



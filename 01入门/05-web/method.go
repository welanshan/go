package main

import (
	"fmt"
	"reflect"
)

type Point struct {
	X,Y int
}

func (p Point) Sum() int {
	return p.X + p.Y
}

func main() {

	// 声明的类型是 Point 实例 需要用到 * 取到实际指针指向的值 
	var p Point
	p = *new(Point)
	p.X = 100
	p.Y = 200
	// ---------------------------------
	var p2 *Point 
	p2 = new(Point)
	p2.X = 300
	p2.Y = 300
	fmt.Printf("sum: %d \n",p2.Sum())

	
	fmt.Println(reflect.TypeOf(p))
	fmt.Println(reflect.TypeOf(p2))
	// ------------------------
	// 第一种写法 实例化并赋值 用 := 操作符
	//p := Point {100,200}

	fmt.Printf("sum: %d",p.Sum())

	
}

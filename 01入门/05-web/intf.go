// 接口 interface 

package main 

import (
	"fmt"
)

// 接口是一种抽象类型，这种类型可以让我们以同样的方式来处理不同的固有类型，不用关心它们的具体实现，而只需要关注它们提供的方法
type Animal interface {
	DoRun()
}

type Dog struct {
	Name string
	Age int
}

func (d Dog) DoRun() {
	fmt.Printf("%s is runing...", d.Name)
}

func main(){
	dog := Dog{Name:"Jerry"}
	dog.DoRun()	
}
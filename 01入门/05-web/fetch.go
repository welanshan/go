//  Fetch prints the content found at a URL.
package main 

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

// go run fetch.go http://www.baidu.com
func main() {
	url := os.Args[1]

	resp,err := http.Get(url)
	if err !=nil {
		fmt.Println(err)
		os.Exit(1)
	}

	b, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	fmt.Println(string(b))
}
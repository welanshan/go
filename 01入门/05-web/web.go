// server1 is a minimal "echo" server 
package main

import (
	"fmt"
	"net/http"
	"log"
)
// http://localhost:8000/hello
func main(){
	http.HandleFunc("/",handler)

	fmt.Println("start a server localhost:8000")
	// 善用go doc 命令
	// go doc http.ListenAndServe
	// ListenAndServe always returns a non-nil error.
	err := http.ListenAndServe("localhost:8000",nil)
	log.Fatal(err)	
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Println(r.URL.Path)
	fmt.Fprintf(w,"URL.Path = %q\n", r.URL.Path)
}
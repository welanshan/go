package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
	"time"
)

//var palette = []color.Color{color.White,color.Black} // 调色板
var rgb = color.RGBA{255, 210, 100, 6}
var palette = []color.Color{rgb,color.Black}

const (
	whiteIndex = 0
	blackIndex = 1
)
// lissa.exe >out.gif
// go run lissa.go >out2.gif
func main(){
	rand.Seed(time.Now().UTC().UnixNano()) // ? 
	lissa(os.Stdout)
}

func lissa(out io.Writer) {
	const (
		cycles =5 // number of complete x oscillator revolutions
		res = 0.001 // angular resolution
		size = 100 // image canvas covers [-size..+size]
		nframes = 64 // number of animation frames
		delay =8 // delay between frames in 10ms units
	)

	freq := rand.Float64() * 3.0 // 
	gf := gif.GIF{LoopCount:nframes}
	phase := 0.0 // phase difference 

	for i:=0;i< nframes;i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
        img := image.NewPaletted(rect, palette)
        for t := 0.0; t < cycles*2*math.Pi; t += res {
            x := math.Sin(t)
            y := math.Sin(t*freq + phase)
            img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5),
                blackIndex)
        }
        phase += 0.1
        gf.Delay = append(gf.Delay, delay)
        gf.Image = append(gf.Image, img)
	}

	gif.EncodeAll(out,&gf)

}
package main

import "fmt"

func main(){
	fmt.Println(gcd(20,1334))
	fib(20)
}

// 求两个值的最大公约数
func gcd(x, y int) int {
	for y != 0 {
		x, y = y, x % y
	}

	return x
}

// 者是计算斐波纳契数列（Fibonacci）的第N个数
func fib(n int) int {
	x, y := 0, 1
	for i:=0;i<n;i++ {
		x,y = y, x+y
		fmt.Printf("%d  ", x)
	}

	return x
}
package main

import (
	"fmt"
	"gotmp/tmp" // 需要go.mod 指定项目名称
	g "gotmp/tmp" // 别名 
)

type student struct {
	name string
	age int
}

// 格式化输出 类似C# 或 java 中重写ToString() 方法
func (s student) String() string {
	// string print formate
	return fmt.Sprintf("{\"name\":%s,\"age\":%d}", s.name,s.age)
}

//type log tmp.Log
// 只想用小写的方法
func log(t g.T) {
	g.Log(t)
}

func main(){
	tmp.Log("dddd")
	tmp.Log(13342)

	s := student { name: "小明", age: 10}
	tmp.Log(s)
	g.Log("hello world")
	//log(23432)

	log("你好")
	fmt.Println("hello")

	var c g.Celsius
	var f g.Fahrenheit

	log(c == 0) // true
	log(f >= 0) // true
	//log(c == f) // complie error :type mismatch
	log(c ==g.Celsius(f)) // f 被强制转换为Celsius 类型 true

	cc := g.FToC(212.0)
	log(cc.String()) 
	fmt.Printf("%v\n",cc)
	fmt.Printf("%s\n", cc)   // "100°C"
	fmt.Println(cc)          // "100°C"
	fmt.Printf("%g\n", cc)   // "100"; does not call String g% 取数值
	fmt.Println(float64(cc)) // "100"; does not call String
}
// Ftoc prints two Fahrenheit-to-Celsius conversions

package main

import "fmt"

func main(){
	const freezingF,boilingF = 32.0, 212.0
	fmt.Printf("%g°F = %g°C\n", freezingF, fToC(freezingF)) // "32°F = 0°C"
    fmt.Printf("%g°F = %g°C\n", boilingF, fToC(boilingF))   // "212°F = 100°C"
}

// 我所行者皆正义，我所悟者应长生
// 华式度转摄氏度
func fToC(f float64) float64 {
	return (f - 32) * 5 / 9
}

package main

import "fmt"

func print(s interface{}) {
	fmt.Println(s)
}

// go 语言不存在未初始化的变量，默认都赋予对应类型的0值
func main(){
	var s string
	var i int 
	var x float32
	var y float64
	var z interface{}

	// 字符串默认赋值 是 "" 字符串
	// 接口数组等默认是 nil 
	// 零值初始化机制可以确保每个声明的变量总是有一个良好定义的值，因此在Go语言中不存在未初始化的变量。这个特性可以简化很多代码，
	// 而且可以在没有增加额外工作的前提下确保边界条件下的合理行为。例如：
	fmt.Println(s=="") // true
	fmt.Println(i,x,y,z) // 0 0 0 nil


	xx := 1 
	p := &xx  // 取xx指针地址赋值给 p 
	print(*p) // * 取 p 指针地址下的值
	*p = 2 // 等价于 xx = 2
	print(*p) // 指针指向的地址的值修改 所以 xx 的值也变更为 2 
	print(xx)
}


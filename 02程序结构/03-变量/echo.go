// echo4 prints its command-line arguments.

package main 

import (
	"fmt"
	"flag" // command line tool
	"strings"
)

var n = flag.Bool("n", false, "omit trailling newline")
var sep = flag.String("s", " ", "separator")

// o run echo.go -s / a dd ee
// echo.exe -help 
// echo.exe -n false a bb cc
func main(){

	flag.Parse()
	fmt.Print(strings.Join(flag.Args(), *sep))

	if !*n {
		fmt.Println()
	}
}
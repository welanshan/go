
// 模拟 ++i
package main 
import "fmt"

func main() {
	v := 1 
	inc(&v) // v is 2

	fmt.Println(v) // 2
	fmt.Println(inc(&v)) // 3
	fmt.Println(v) // 3
}

func inc(i *int) int {
	*i ++ // 非常重要：只是增加p指向的变量的值，并不改变 i 指针！！！
	return *i
}
